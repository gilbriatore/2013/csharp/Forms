﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Forms
{
    public partial class Form2 : Form
    {
        Form1 f;
        public Form2(Form formulario)
        {
            InitializeComponent();
            f = (Form1)formulario;
        }

        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            f.textBox1.Text = textBox1.Text;
        }
    }
}
